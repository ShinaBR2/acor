<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function home(){
		if(Input::hasFile('file_up') ){
			$file = Input::file('file_up');
			$user_id = 1;

			$reader = Excel::load($file->getRealPath());

	        // Đọc tất cả nội dung file excel
	        $data = $reader->get();
	        // Init array keywords
	        $keywords = [];
	        // Nếu file excel có nhiều sheet, fetch từng sheet
	        if ($data instanceof Maatwebsite\Excel\Collections\SheetCollection) {
	            foreach ($data as $sheet) {
	                $this->parseSheet($sheet, $keywords);
	            }
	        }
	        else {
	        	$worksheetTitle = $data->getTitle();
	        	foreach ($data as $row){
	        		$arr = $row->toArray();
	        		dd($arr);
	        		$kw = $arr['keyword'];
	        		$landing = $arr['landing_page'];
	        		$col1 = $arr['col_1'];
					$col2a = $arr['col_2'];
					$col2 = str_replace("'","&apos;",$col2a);
					$col3 = $arr['col_3'];
					$quali = $arr['quantity'];
					$type = $arr['type'];
					$cat= $user_id;
	        		die();

	        		Anchortext::whereRaw("alter table anchortext AUTO_INCREMENT = 1");
					$anchor = new Anchortext;
					$anchor->category = $cat;
					$anchor->kw = $kw;
					$anchor->landing_page = $landing;
					$anchor->column1 = $col1;
					$anchor->column2 = $col2;
					$anchor->column3 = $col3;
					$anchor->quantity = $quali;
					$anchor->type = $type;
					$anchor->loai = $loai;
					$anchor->status = '';
					$anchor->date_modify = '';
					$anchor->save();/**/
				}
	        }
			
		}
		else{
			$error = "";
		}


		


		$loais = Loai::all();
		$data = array(
			'user_role' => 'administrator',
			'loais' => $loais,

			//'status' => array()
		);


		return View::make('home',$data);
	}

	public function show($loai){
		if(Input::get('ghi_file') != null){
			$user_id = 1;
			$rows = null;	
			if($user_id == 1){
				//$sql="select * from anchortext where loai = '$loai'";

				$rows = Anchortext::where('loai','=',$loai)->get();
			}
			else{
				//$sql="select * from anchortext where category != 1 and loai = '$loai'";
				$rows = Anchortext::whereRaw('category != 1 and loai = '.$loai)->get();
			}


			Excel::create('ketqua', function($excel) use($rows) {
				//$excel->setActiveSheetIndex(0);

			    $excel->sheet('danh sách anchortext', function($sheet) use($rows) {
			        $sheet->setOrientation('landscape');
					$sheet->setCellValue('A1', 'keyword');
					$sheet->setCellValue('B1', 'landing_page');
					$sheet->setCellValue('C1', 'col_1');
					$sheet->setCellValue('D1', 'col_2');
					$sheet->setCellValue('E1', 'col_3');
					$sheet->setCellValue('F1', 'quantity');
					$sheet->setCellValue('G1', 'type');
					$i = 2;
					foreach ($rows as $key => $row) {
						if( (strpos($row->column2, '<')==false) && (strpos($row->column2, '>')==false) && ($row->column2 != "")){
							$row->column2 = '<p>'.$row->column2.'</p>' ;
						}
						$sheet->setCellValue('A'.$i, $row->kw);
						$sheet->setCellValue('B'.$i, $row->landing_page);
						$sheet->setCellValue('C'.$i, $row->column1);
						$sheet->setCellValue('D'.$i, $row->column2);
						$sheet->setCellValue('E'.$i, $row->column3);
						$sheet->setCellValue('F'.$i, $row->quantity);
						$sheet->setCellValue('G'.$i, $row->type);
						$i++;
					}
				})->export('xlsx');
			});
		}
		if(Input::get('submit_delA') != null){
			Anchortext::where('loai','=',$loai)->delete();
			Loai::where('id','=',$loai)->delete();/**/
			return Redirect::action('HomeController@home');
		}
		
		
		$chk_loai = Loai::where('id','=',$loai)->first();
		$row_str_name = Loai::where('id','=',$loai)->first();
		if($chk_loai->status == 1){
			$checked = "checked";
		}
		else{
			$checked="";
		}

		$num_title = 0;
		$num_des = 0;
		$nline=25;
		if ((Input::get('pag') == null)||(Input::get('pag') == 1)){
			$start=0;
		}
		else {
			$start = (Input::get('pag') - 1)*$nline;
		}
		$p=Input::get('pag');


		/*if($user_role == 'administrator'){
			$sql="SELECT * FROM anchortext WHERE loai = '$loai' LIMIT $start,$nline";
			$sql_total="SELECT * FROM anchortext WHERE loai = '$loai'";
		}
		else{
			//$sql="SELECT * FROM anchortext WHERE category='$user_id' and loai = '$loai' LIMIT $start,$nline";
			//$sql_total="SELECT * FROM anchortext WHERE category='$user_id' and loai = '$loai'";
			$sql="SELECT * FROM anchortext WHERE category !=1 and loai = '$loai' LIMIT $start,$nline";
			$sql_total="SELECT * FROM anchortext WHERE category !=1 and loai = '$loai'";
		}
		$result=mysql_query($sql);
		$result_total=mysql_query($sql_total);
	    $total=mysql_num_rows($result_total);/**/
	    
	    

	    $result = Anchortext::where('loai','=',$loai)->limit($nline)->offset($start)->get();
	    //$result = Anchortext::where('loai','=',$loai)->get();
		$total = count(Anchortext::where('loai','=',$loai)->get());
		$npage=(($total % $nline)==0) ? $total/$nline:floor($total/$nline)+1;
		if(Input::get('pag') != null)
			$page_ht = Input::get('pag');
		else
			$page_ht = 1;

		/**/

		return View::make('show', compact('result', 'row_str_name', 'loai','chk_loai','checked','npage','page_ht','loai','p'));
	}
	public function ajax(){
		//echo Input::get('col2');
		echo "Action ajax";

		
			/*include_once('connect.php');
			//update anchortext
			die(":sax");/**/

		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$date = date('Y-m-d G:i:s');/**/



		if(Input::get('id') != null){
			$id_edit=Input::get('id');
			$kw = Input::get("kw");
			$col2a = Input::get("col2");
			$col2 = str_replace("'","&apos;",$col2a);
			$col1 = Input::get("col1");

			$anchortext = Anchortext::where('id','=',$id_edit)->update(array('kw' => $kw,'column1' => $col1,'column2' => $col2, 'date_modify' => $date));

			$id_loai = Anchortext::where('id','=',$id_edit)->select('loai')->first()->loai;
			Loai::where('id','=',$id_loai)->update(array('date_modify' => $date));
			echo "Upload Thành công";



			/*$edit="UPDATE anchortext SET kw='$kw', column1='$col1', column2='$col2', date_modify='$date' WHERE id='$id_edit'";
			mysql_query($edit) or die(mysql_error());

			$result_edit = mysql_query("Select loai from anchortext where id = '$id_edit'");
			$row_edit = mysql_fetch_array($result_edit);
			$id_loai= $row_edit['loai'];

			mysql_query("UPDATE loai SET date_modify = '$date' WHERE id='$id_loai'") or die(mysql_error());

			echo "Upload Thành công";/**/
		}
		//////////////

		//xóa anchortext

		if(Input::get('idxoa') != null){
			$idxoa = Input::get('idxoa');
			Anchortext::where('id','=',$idxoa)->delete();
			echo "Xóa thành công";
			/*$del = "delete from anchortext where id='$idxoa'";
			mysql_query($del) or die(mysql_error());
			echo "Xóa thành công";/**/
		}
		//////////////

		//duyệt loại
		if(Input::get('id_status') != null){
			$id_status = Input::get('id_status');
			$status = Input::get('val_check');
			Loai::where('id','=','$id_status')->update(array('status' => $status));
			//mysql_query("UPDATE loai SET status =$status WHERE id =$id_status") or die(mysql_error());
			if($status==1)
				echo "Đã duyệt chủ đề này";
			elseif($status==0)
				echo "Đã hủy duyệt chủ đề này";
		}
		/////////////////////

		//duyệt anchortext

		if(Input::get('id_duyet') != null){
			$chk_duyet = Input::get('chk_duyet');
			$id_duyet = Input::get('id_duyet');

			//mysql_query("UPDATE anchortext SET status = '$chk_duyet' WHERE id ='$id_duyet'") or die(mysql_error());

			Anchortext::where('id','=',$id_duyet)->update(array('status' => $chk_duyet));
			if($chk_duyet==1)
				echo "Đã duyệt bài";
			elseif($chk_duyet==0)
				echo "Hủy duyệt bài";
		}
		/////////////////////


		//tìm kiếm
		if (isset($_GET['name'])) {
			$data = "%".$_GET['name']."%";
			//echo "dsadsa";
			$user_role = ($_GET['user_role']);
			if($user_role == 'administrator'){
				$sql="SELECT * FROM loai, anchortext WHERE loai.id = anchortext.loai AND name like '$data' GROUP BY name";
			}
			else{
				$sql="SELECT * FROM loai, anchortext WHERE loai.id = anchortext.loai AND category!=1 AND name like '$data' GROUP BY name";
			}
			$results= mysql_query($sql) or die(mysql_error());

			if(empty(mysql_num_rows($results))) {
				echo '<tr>
				<td colspan="6">Không tìm thấy kết quả nào</td>
				</tr>';
			}
			else {
				//echo "aaaaa";
				echo '<tr>
							<th class="header table-header-repeat line-left minwidth-1"><a>Tên Chủ Đề</a></th>
							<th class="header table-header-repeat line-left minwidth-1"><a>Số lượng</a></th>
							<th class="header table-header-repeat line-left minwidth-1"><a>User</a></th>
							<th class="header table-header-repeat line-left minwidth-1"><a>Modify</a></th>
							<th class="header table-header-repeat line-left"><a>Trạng thái</a></th>

						</tr>';
				$i=0;
				while ($rows_tim = mysql_fetch_array($results)) {
					$loai = $rows_tim['loai'];
					//$loai = $rows_tim['loai'];
					$cat = $rows_tim['category'];
					$str_sl = "SELECT id FROM anchortext WHERE loai = '$loai'";
					$result_str = mysql_query($str_sl);
					$soluong = mysql_num_rows($result_str);
					$str_ten="SELECT user_nicename FROM wp_users, anchortext WHERE wp_users.id = '$cat'";
					$result_ten = mysql_query($str_ten);
					$row_ten = mysql_fetch_array($result_ten);
					?>
					<tr class="<?php //echo $status;?> contents">
						<td class="options-width"><label><a href="http://anc.mangoads.vn/detail-anchortext?loai=<?php echo $rows_tim['loai'];?>"><?php echo $rows_tim['name'];?></a></label></td>
						<td><label><?php echo $soluong;?></label></td>
						<td><label><?php echo $row_ten['user_nicename'];?></label></td>
						<td><label><?php echo $rows_tim['date_modify'];?></label></td>
						<td>
						</td>
					</tr>
				<?php
				}
			}
		}

		/////////////////////

		//duyệt việc dán anchortext?

		if(Input::get('change_loai') != null){
			$id_loai = Input::get('change_loai');
			$status_pass = Input::get('chk_duyet');

			//mysql_query("UPDATE loai SET status_pass = '$status_pass' WHERE id ='$id_loai'") or die(mysql_error());
			Loai::where('id','=',$id_loai)->update(array('status_pass' => $status_pass));

			echo "da thanh cong";
		}
	}

}
