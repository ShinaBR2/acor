<?php


class Anchortext extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'anchortext';
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function owner(){
		return $this->belongsTo('User', 'category');
	}
}