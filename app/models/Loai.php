<?php


class Loai extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'loai';
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public function anchors(){
		return $this->hasMany('Anchortext', 'loai');
	}

	public function not_translate(){
		return $this->hasMany('Anchortext', 'loai')->where('column2', '');
	}

	
}