<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@home'));
Route::post('/', array('as' => 'home', 'uses' => 'HomeController@home'));
Route::get('/detail-anchortext/{loai?}', array('as' => 'show', 'uses' => 'HomeController@show'));
Route::post('/detail-anchortext/{loai?}', array('as' => 'show', 'uses' => 'HomeController@show'));
Route::post('/ajax-luu-anchortext', array('as' => 'ajax', 'uses' => 'HomeController@ajax'));


Route::get('/test', function(){
	$user = Anchortext::find(7720);
	return $user->owner;
});

/*
ajax-luu-anchortext.php
ajax-luu-anchortext2.php
connect.php
detail.xuly.php
detail-anchortext.php
detail-anchortext2.php
doc_file_excel.php
doc_file_excel3.php
page-anchortext.php
page-anchortext-3.php
tao_file_excel.php
tao_file_excel3.php

/**/