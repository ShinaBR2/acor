<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Anchortext «  Trang trí nội thất</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name = "format-detection" content = "telephone=no">
	<link href="{{ asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" id="open-sans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=3.9.1" type="text/css" media="all">
    <link rel="stylesheet" id="open-sans-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=3.9.1" type="text/css" media="all">

    <link href="{{ asset('css/dashicons.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/admin-bar.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/screen.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/check-dan.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jquery.datepick.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/ckeditor/skins/moono/editor.css')}}" rel="stylesheet" type="text/css">
	<style type="text/css" media="screen">
		html { margin-top: 32px !important; }
		* html body { margin-top: 32px !important; }
		@media screen and ( max-width: 782px ) {
			html { margin-top: 46px !important; }
			* html body { margin-top: 46px !important; }
		}
	</style>
	<script src="{{ asset('js/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('js/jquery/jquery-migrate.min.js')}}"></script>
    <script src="{{ asset('js/iosOverlay.js')}}"></script>


    <script src="{{ asset('js/jquery/ui/jquery.ui.core.min.js')}}"></script>
    <script src="{{ asset('js/jquery/ui/jquery.ui.widget.min.js')}}"></script>
    <script src="{{ asset('js/jquery/ui/jquery.ui.tabs.min.js')}}"></script>
    <script src="{{ asset('js/jquery.hoverIntent.minified.js')}}"></script>
    <script src="{{ asset('js/css3-mediaqueries.js')}}"></script>
    <script src="{{ asset('js/tabs.js')}}"></script>
    <script src="{{ asset('js/admin-bar.min.js')}}"></script>
    <script src="{{ asset('js/comment-reply.min.js')}}"></script>
    <!--<script src="{{ asset('js/jquery/jquery.datepick.js')}}"></script>-->
    <!--<script src="{{ asset('js/ownScript.js')}}"></script>-->
    <script src="{{ asset('js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ asset('js/ckeditor/lang/en.js')}}"></script>
    <script src="{{ asset('js/shortcodes.js')}}"></script>
    <!--<script src="{{ asset('js/superfish.js')}}"></script>-->


    

    
    



    
<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0; url=detect.html" />
	<script type="text/javascript">
	/* <![CDATA[ */
	window.top.location = 'detect.html';
	/* ]]> */
	</script>
<![endif]-->



</head>



</head>
<body>

<div id="page">
	<div id="header" role="banner">
		<div id="headerimg">
			<h1><a href="{{URL::action('HomeController@home') }}">Trang trí nội thất</a></h1>
			<div class="description"></div>
		</div>
	</div>

	<link href="{{ asset('css/jquery.dataTables.css')}}" rel="stylesheet" type="text/css">
	<script src="{{ asset('js/jquery.dataTables.js')}}"></script>
	

	<div id="content-outer">
		<div id="content" style="background:#fff">
			

			@yield('content')
		</div>
		<!--  end content-table  -->
	</div><!-- div id=content-outer"-->

</div><!-- div id="page"-->


</body>
</html>