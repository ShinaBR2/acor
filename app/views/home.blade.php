@extends('layouts.main')

@section('content')

<?php
	if(isset($error)){
		//echo $error.'Loi day neeeeeeeee      eeeeeeeeeeeeeeeeeee';
	}
?>


<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		$('#product-table').dataTable( {
			"order": [[ 3, "desc" ]]
		} );
	} );
</script>

<div id="page-heading" style="height:50px;width:100%;">
	<h1 style="float:left;">Danh sách Project</h1>
</div>
<div id="head_form">
	<form action="" method="post" id="frm1" enctype="multipart/form-data">
		<label style="font-size:16px;font-family:Times New Roman;width:80px;">Tên Project </label>
		<input type="text" name="txt_name" id="txt_name" value=""/>
		<label style="font-size:16px;font-family:Times New Roman;width:80px;">Anchor </label>
		<input style="float:left;" type="file" name="file_up" id="file_up" value=""/>
		<input type="submit" name="doc_file" id="doc_file" value="Load File"/>
	</form>
	<!--<form method="get" action="" id="frm_tim">
		<label for="name">Từ khóa:</label>
		
		<input type="button" class="btnSearch" value="Tìm" name="btn_search" id="btn_search"/>   
	</form>-->
	<div style="float:right;margin-right:50px;">
		<label style="font-size:20px;">User - <?php echo 'admin';/*echo $current_user -> display_name; /**/?></label>
	</div>
</div>

<div id="content-table-inner">
	<div id="table-content">
		<form id="mainform_anchor" action="" method="post" enctype="multipart/form-data">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" class="tablesorter" id="product-table">
				<thead>
					<tr>
						<th class="header">Tên Project</th>
						<th class="header">Số bài</th>
						<th class="header">Bài chưa viết</th>
						<th class="header">User</th>
						<th class="header">Ngày cập nhật</th>
						<th class="header">Trạng thái</a></th>
						@if($user_role == 'administrator')
							<th class="header">Test Dán anchortext</th>
						@endif									
					</tr>
				</thead>
				<tbody>
					@foreach ($loais as $i => $loai)
						<tr>
							<td class="options-width">
								<label><a href="{{URL::action('HomeController@home') }}/detail-anchortext/{{$loai->id}}" title="">{{$loai->name}}</a></label>
								<div style="display:none;position:absolute;overflow:auto;"></div>
							</td>
							<td><label>{{count($loai->anchors)}}</label></td>
							<td width="150px" align="center">
								@if (count($loai->not_translate()->get()) == 0)
									<span class="check">Đã viết xong</span>
								@else 
									<span style="font-size:16px;">{{count($loai->not_translate()->get())}}</span>
								@endif
								
							</td>
							<td>
								<label>
								@if (count($loai->anchors) > 0)
									@if($loai->anchors[0]->category == 1)
										<font style='color:red'>{{ $loai->anchors[0]->owner->display_name }}</font>
									@else
										{{ $loai->anchors[0]->owner->display_name }}
									@endif

								@endif
								</label>
							</td>
							<td><label>{{ $loai->date_modify }}</label></td>
							<td class="trang-thai">
								@if($loai->status == 1)
									<span class='check'>Đã duyệt</span>
								@else
									<span class='not-check'>Chưa duyệt</span>
								@endif
							</td>
							@if($loai->status_pass == 1)
								<?php $status_pass = 'checked' ?>
							@else
								<?php $status_pass = ''; ?>
							@endif
							@if ($user_role == 'administrator')
							<td align="center"data-id={{$loai->id}}>
								<div class="switch">
									<input id="cmn-toggle-{{$i}}" class="cmn-toggle cmn-toggle-yes-no" type="checkbox" {{$status_pass}} data-id={{$loai->id}}>
									<label for="cmn-toggle-{{$i}}" data-on="Đã dán" data-off="Chưa dán"></label>
			  					</div>
			  				</td>
				  			@endif
						</tr>
					@endforeach
				</tbody>
			</table>
		</form>
	</div><!--  end content-table  -->
</div>











<script type="text/javascript">
	$(document).ready(function() {
		$('.cmn-toggle').change(function() {
			if($(this).is(":checked")){
				var id_loai = $(this).attr('data-id');
				$.ajax({
					url: "{{URL::action('HomeController@ajax') }}",
					data: {chk_duyet: 1, change_loai: id_loai},
					type: 'POST',
					dataType: "html",
					success: function(html){
						iosOverlay({
							text: "Đã duyệt!",
							duration: 700,
							icon: "{{asset('images/check.png') }}"
						});
						//alert(html);
						//idxoa.parent().parent().remove();
					}
				});
			}
			else{
				var id_loai = $(this).attr('data-id');
				$.ajax({
					url: "{{URL::action('HomeController@ajax') }}",
					data: {chk_duyet: 0, change_loai: id_loai},
					type: 'POST',
					dataType: "html",
					success: function(html){
						iosOverlay({
							text: "Hủy duyệt bài!",
							duration: 700,
							icon: "{{asset('images/check.png')}}"
						});
						//alert(html);
						//idxoa.parent().parent().remove();
					}
				});
			}/**/
		});
	});
</script>





@stop