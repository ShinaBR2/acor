@extends('layouts.main')

@section('content')
<?php
	//error_reporting(E_ALL ^ E_NOTICE);
?>
<?php /* Need to be check user here /**/ ?>


<script>
 $(document).ready(function(){
	$("#btn_cn").click(function(){
		if($("#chk_status").is(':checked'))
			var val_check = 1;
		else
			val_check = 0; 
		var id_status = '{{$loai}}';
		//alert("aaa");
		jQuery.ajax({
			url: "{{URL::action('HomeController@ajax') }}",
			data: {val_check: val_check, id_status: id_status},
			type: 'POST',
			dataType: "html",
			success: function(html){
				iosOverlay({
					text: "Đã cập nhật!",
					duration: 700,
					icon: "{{asset('images/check.png')}}"
				});
			},
			error: function(loi){
				iosOverlay({
					text: "Lỗi cập nhật!",
					duration: 700,
					icon: "{{asset('images/check.png')}}"
				});
			}
		});
	});
});
 </script>

<link href="{{ asset('css/iosOverlay.css')}}" rel="stylesheet" type="text/css">
<script src="{{ asset('js/iosOverlay.js')}}"></script>
<script src="{{ asset('js/custom.js')}}"></script>





<div id="page-heading">
	<h1>Danh sách anchortext tại project {{$row_str_name->name}}</h1>
</div>

<div id="head_form">
	<form action="" method="post" id="frm1" enctype="multipart/form-data">		
	<input id="ghi_file" name="ghi_file" value="Download File Excel" type="submit"/>
	
	<?php
	





		/*if($user_role == 'administrator' || $user_id == $row_id['category']){/**/


	?>


	<input style="margin-right:200px;" id="submit_delA" name="submit_delA" value="Xóa tất cả" type="submit" onclick="return confirm('Bạn có chắc là muốn xóa toàn bộ dữ liệu cũ ko?');"/>
	

	<?php /*} /**/?>
	</form>
	
	<?php 
	//if($user_role == 'administrator'){}
	?>
	<div style="margin-left:200px;">
	<input class="css-checkbox" type="checkbox" name="chk_status" id="chk_status" {{$checked}}/>
	<label class="css-label" for="chk_status">Đã duyệt</label>
	<input type="button" name="btn_cn" id="btn_cn" value="Cập nhật"/>


	<?php //} ?>
	

	</div>
	<div style="float:right;margin-right:50px;">
		<label style="font-size:20px;">User - <?php //echo $current_user -> user_login; ?></label>
	</div>

</div>
<!-- end page-heading -->

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

	<tr>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
	</tr>

	<tr>
		<td id="tbl-border-left"></td>
		<td>
			<div id="content-table-inner">
				<div id="table-content">
					<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">

						<tr>
							<th class="table-header-repeat line-left minwidth-1"><a>STT</a></th>
							<th class="table-header-repeat line-left minwidth-1"><a>KW</a></th>
							<th class="table-header-repeat line-left minwidth-1"><a>Landing_page</a></th>
							<th class="table-header-repeat line-left minwidth-1"><a>Column 1</a></th>
							<th class="table-header-repeat line-left minwidth-1"><a>Column 2</a></th>
							<th class="table-header-repeat line-left"><a>Xử lý</a></th>
						</tr>



						@foreach ($result as $index => $row)
							<tr data-id={{$row->id}} data-index="{{$index}}" class="dong_{{$index+1}}">
								<td>
									<label>
										@if(isset($p))
											{{(($p-1)*25)+($index+1)}}
										@else
											{{$index+1}}
										@endif
									</label>
								</td>
								<td>{{ Form::text('txt_kw', $row->kw, ['class' => 'txt_kw_$index']) }}</td>
								<td><span style="color:red;word-wrap:break-word">{{$row->landing_page}}</span></td>
								<td>
									<input type="text" name="txt_col1" class="txt_col1{{$index}} txtcol1iii" id="txt_col1{{$index}}" value="{{$row->column1}}" />
									<br/>
									<label>Số từ: <span id="tbao{{$index}}">{{count(explode(' ', $row->column1))}}</span></label>
								</td>
								<td id="editor_area" class="editor_area_{{$index}}">
									<textarea name="txta_col2" id="txta_col2{{$index}}" class="ckeditor" width="650px">{{$row->column2}}</textarea>
									<br/>
								</td>
								<td>
									<input type="button" class="ct_anchor luu_ajax_{{$index}} lajiii" name="luu_ajax" value="Lưu" id="luu_ajax_{{$index}}"/>
									<input type="text" class="txt_id_{{$index}}" id="txt_id{{$index}}" name="txt_id" value="{{$row->id}}" style="display:none"/>
									<?php //if($user_id == $row['category'] || $user_role == 'administrator') {?>
									<input type="button" class="ct_anchor btn_del{{$index}} btndeliii" name="btn_del" value="Xóa" id="btn_del" onclick=""/>

									@if($row->status == 1)
										<?php $duyet = "checked";?>
									@else 
										<?php $duyet =""; ?>
									@endif

									<input type="checkbox" value="1" name="duyet" id="duyet_{{$index}}" {{$duyet}} class="css-checkbox duyet_{{$index}} dchk" /><label for="duyet_{{$index}}" name="demo_lbl" class="css-label">Duyệt</label>
									<?php //} ?>
									</td>	
								</tr>
						@endforeach

						<?php
						
						?>

						<tr>
							<td colspan="6">
								<div class="pagenavi" style="float:right;margin-right:150px;">
									<a href="{{URL::action('HomeController@home') }}/detail-anchortext/{{$loai}}?pag=1" class="page-left" title="Đầu Tiên"></a>

									<div id="page-info">Trang - 
										@for ($i = 1; $i <= $npage; $i++)
											@if($page_ht == $i)
												<a><b style='font-size:18px;color:rgb(199, 30, 206)'>{{$i}}</b></a>
											@else
												<a href="{{URL::action('HomeController@home') }}/detail-anchortext/{{$loai}}?pag={{$i}}" title="">{{$i}}</a>
											@endif
										@endfor
									</div>

									<a href="{{URL::action('HomeController@home') }}/detail-anchortext/{{$loai}}?pag={{$npage}}" class="page-far-right" title="Cuối Cùng"></a>
								</div>
							</td>
						</tr>
					</table><!--  end product-table................................... --> 

				</div>
				<!--  end content-table  -->
			</div>
			<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
</table>

<!--  end content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('.dchk').change(function() {
			if($(this).is(":checked")){
			var id_duyet = $(this).parent().parent().attr('data-id');
				jQuery.ajax({
					url: "{{URL::action('HomeController@ajax') }}",
					data: {chk_duyet: 1, id_duyet: id_duyet},
					type: 'POST',
					dataType: "html",
					success: function(html){
						iosOverlay({
							text: "Đã duyệt!",
							duration: 700,
							icon: "{{asset('images/check.png')}}"
						});
					}
				});
			}
			else{
				var id_duyet = $(this).parent().parent().attr('data-id');
				jQuery.ajax({
					url: "{{URL::action('HomeController@ajax') }}",
					data: {chk_duyet: 0, id_duyet: id_duyet},
					type: 'POST',
					dataType: "html",
					success: function(html){
						iosOverlay({
							text: "Hủy duyệt bài!",
							duration: 700,
							icon: "{{asset('images/check.png')}}"
						});
					}
				});
			}
		});
		
		$(".txtcol1iii").keydown(function(){
			var dem2 = (($(this).val()).split(" ")).length;
			$(this).parent().find('label').find('span').html(dem2);
		});

		$(".lajiii").click(function(){
			var i = $(this).parent().parent().attr('data-index');
			var tmp = 'txta_col2'+i;
			var col2 = CKEDITOR.instances[tmp].getData();
			var kw= $('.txt_kw_'+i).val();
			var col1= $('#txt_col1'+i).val();
			var id=$('.txt_id_'+i).val();
			$.ajax({
				url: "{{URL::action('HomeController@ajax') }}",
				data: {col2: col2, kw: kw, col1: col1, id: id},
				type: 'POST',
				dataType: "html",
				success: function(html){
					iosOverlay({
						text: "Đã lưu!",
						duration: 700,
						icon: "{{asset('images/check.png')}}"
					});
					console.log(html);
				}
			});
		});
		
		$(".btndeliii").click(function(){
			var pr = $(this).parent().parent();
			var idxoa=$(this).parent().parent().attr('data-id');
			if (confirm('Bạn có chắc là muốn xóa ko?')){
				jQuery.ajax({
				url: "{{URL::action('HomeController@ajax') }}",
				data: {idxoa: idxoa},
				type: 'POST',
				dataType: "html",
				success: function(html){
					pr.remove();
					iosOverlay({
						text: "Đã xóa!",
						duration: 700,
						icon: "{{asset('images/check.png')}}"
					});
					console.log(html);
				}
			});
			}
		});
		
	});
</script>
@stop